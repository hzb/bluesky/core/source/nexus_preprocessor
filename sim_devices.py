from ophyd.sim import SynGauss, SynAxis
from ophyd import Signal, Component as Cpt
from ophyd import Device
from nexus_dictionaries import NxDict


class MyDet(SynGauss):
    """A simulated detector modified to accept an additional parameter, 
    the nx_class

    Args:
        nx_class_val (NxDict): _description_
    """    
    def __init__(self,name, motor, motor_field, nx_class_val:NxDict, **kwargs):
        self._nx_class_val = nx_class_val
        super().__init__(name, motor, motor_field, **kwargs)
        self.nx_class = self._nx_class_val


class SMU(Device):
    def __init__(self, prefix="", *, name, nx_class_val:NxDict, kind=None, read_attrs=None, configuration_attrs=None, parent=None, **kwargs):
        self._nx_class_val = nx_class_val
        super().__init__(prefix, name=name, kind=kind, read_attrs=read_attrs, configuration_attrs=configuration_attrs, parent=parent, **kwargs)
        self.nx_class= self._nx_class_val
        
    tx   = Cpt(SynAxis, name='tx')
    ty   = Cpt(SynAxis, name='ty')
    rx   = Cpt(SynAxis, name='rx')
    ry   = Cpt(SynAxis, name='ry')
    rz   = Cpt(SynAxis, name='rz')


class PGM(Device):
    def __init__(self, prefix="", *, name, nx_class_val:NxDict, kind=None, read_attrs=None, configuration_attrs=None, parent=None, **kwargs):
        self._nx_class_val = nx_class_val
        super().__init__(prefix, name=name, kind=kind, read_attrs=read_attrs, configuration_attrs=configuration_attrs, parent=parent, **kwargs)
        self.nx_class= self._nx_class_val
        
    en   = Cpt(SynAxis, name='en')
    cff   = Cpt(SynAxis, name='ty')



class Mirror(Device):
    def __init__(self, prefix="", *, name, nx_class_val:NxDict, kind=None, read_attrs=None, configuration_attrs=None, parent=None, **kwargs):
        self._nx_class_val = nx_class_val
        super().__init__(prefix, name=name, kind=kind, read_attrs=read_attrs, configuration_attrs=configuration_attrs, parent=parent, **kwargs)
        self.nx_class= self._nx_class_val
        
    tx   = Cpt(SynAxis, name='tx')
    ty   = Cpt(SynAxis, name='ty')
    tz   = Cpt(SynAxis, name='tz')
    rx   = Cpt(SynAxis, name='rx')
    ry   = Cpt(SynAxis, name='ry')
    rz   = Cpt(SynAxis, name='rz')

class Aperture(Device):
    def __init__(self, prefix="", *, name, nx_class_val:NxDict, kind=None, read_attrs=None, configuration_attrs=None, parent=None, **kwargs):
        self._nx_class_val = nx_class_val
        super().__init__(prefix, name=name, kind=kind, read_attrs=read_attrs, configuration_attrs=configuration_attrs, parent=parent, **kwargs)
        self.nx_class= self._nx_class_val
        
    top    = Cpt(SynAxis, name='top')
    bottom = Cpt(SynAxis, name='bottom')
    left   = Cpt(SynAxis, name='left')
    right  = Cpt(SynAxis, name='right')

class ExitSlit(Device):
    def __init__(self, prefix="", *, name, nx_class_val:NxDict, kind=None, read_attrs=None, configuration_attrs=None, parent=None, **kwargs):
        self._nx_class_val = nx_class_val
        super().__init__(prefix, name=name, kind=kind, read_attrs=read_attrs, configuration_attrs=configuration_attrs, parent=parent, **kwargs)
        self.nx_class= self._nx_class_val
        
    vgap        = Cpt(SynAxis, name='vgap')
    hgap        = Cpt(SynAxis, name='hgap')
    translation = Cpt(SynAxis, name='translation')
