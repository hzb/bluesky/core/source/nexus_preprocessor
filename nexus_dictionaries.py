class NxDict:
    """
    A base class for creating dictionaries with default values 
    and filtering based on certain conditions.
    """
    default_values = {
        'world_position': None,
    }

    def __init__(self, **kwargs) -> None:
        """
        Initializes the NxDict object with default values and 
        updates it with keyword arguments.
        
        Args:
            **kwargs: Keyword arguments to set or override values in the dictionary.
        """
        self.__dict__.update(self.default_values)
        self._filter_false_values(kwargs)

    def _filter_false_values(self, kwargs):
        """
        Filters out keys with False values and updates the dictionary.

        Args:
            kwargs (dict): The dictionary to filter and update.
        """
        for key, value in list(kwargs.items()):
            if value is False:
                del self.__dict__[key]
            elif value is not None:
                self.__dict__[key] = value

    def to_dict(self):
        """
        Returns a dictionary with non-False.
        
        Returns:
            dict: A filtered dictionary.
        """
        return {k: v for k, v in self.__dict__.items() if v is not False}
    
    def none_keys(self):
        """
        Returns a list of keys with None values in the dictionary.
        
        Returns:
            list: A list of keys with None values.
        """
        none_keys = [key for key, value in self.to_dict().items() if value is None]
        return none_keys
    
    def update_values(self, oe):
        """
        Updates dictionary values based on a given object's signals.
        
        Args:
            oe: An object with signals to update the dictionary.
        """
        for key in self.none_keys():
            for sign in oe._signals.keys():
                if sign == key:
                    self.__dict__[key] = oe._signals[sign].readback.get()


class NxDictMirror(NxDict):
    """
    A class representing NxDict for a Mirror.
    """
    default_values = {
        'world_position': None,
        'coating': None,
        'incident_angle': None,
        'tx': None,
        'ty': None,
        'tz': None,
        'rx': None,
        'ry': None,
        'rz': None,
    }


class NxDictExitSlit(NxDict):
    """
    A class representing an NxDict for exit slit.
    """
    default_values = {
        'world_position': None,
        'en': None,
        'cff': None,
    }

class NxDictAperture(NxDict):
    """
    A class representing an NxDict for exit slit.
    """
    default_values = {
        'world_position': None,
        'top': None,
        'bottom': None,
        'left': None,
        'right': None,
    }

class NxDictExitSlit(NxDict):
    """
    A class representing an NxDict for exit slit.
    """
    default_values = {
        'world_position': None,
        'horizontal_gap': None,
        'vertical_gap': None,
    }


class NxDictPGM(NxDict):
    """
    A class representing an NxDict for DCM monochromator.
    """
    default_values = {        
        'world_position': None,
        'en': None,
        'cff': None, 
    }


class NxDictKeythley(NxDict):
    """
    A class representing an NxDict for Keithley instrument.
    """
    default_values = {
        'world_position': None,
        'signal_source': None,    
    }
