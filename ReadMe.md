# Nexus metadata creator
In this project I prototype a nexus metadata creator. To run the example run the two cells in the `nexus_md.ipynb`. The .venv environmen is included in the project to be able to experiment with it promptly.

## sim_devices.py
In this file some two devices are created: one to simulate a mirror, and one to simulate a detector. They wil not be needed in the final version and are created only for testing purposes.

## nexus_dictionaries.py
In this file the classes to create nexus metadata are defined. The base class  `NxDict` is defined. as default values has only the 'world_position', that is common to all the elements. Additionally it has a method called `to_dict()` the returns a dictionary using the `default_values`. If an element of the `default_values` is set to `False` it is filtered out from the dictionary. If a default value is set to `None`, it will be updated by the `update_values` method. For the method to work, it is essential that the name of the axis is the same as the one defined in the Device. Examples for Mirrors, DCM monochromator and Keithley are given. 

## supplemental_data_nexus.py
In this file the `SupplementalDataNexus` are defined. These supplmental data are responsible for creating the nexus metadata dictionray and adding it to the Start document. Everytime a plan is performed, the function `populate_nexus_dictionary` is called and it populates the nexus metadata dictionary everytime an item with `None` value is found. 

## nexus_md.ipynb
This is a jupyter notebook that demonstrate how to use the `SupplementalDataNexus` and how to use the `NxDict` class and its derivates.
